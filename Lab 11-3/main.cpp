/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 27, 2014, 5:10 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 11-3
 */
int main(int argc, char** argv) {
     
   int array[10];     
    int k = 0;
    int num = 0 ;
    
    for( int i = 1; i < 11; i++)
    {
       cout << "Enter array # "<< i <<" :";
       cin >> array[i];
       
       if( array[i] == -1 )
       {
          break;
       }       
       cout << "Array "<< i << " is "<< array[i]<< endl;
       k++;       
         num += array[i];      
    }
    cout <<"The sum of the arrays is "<<num<< endl;
    int average = num/k ;
    cout << "Average " << average;
    return 0;    
    
}        
      