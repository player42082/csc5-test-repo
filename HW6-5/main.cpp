/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 15, 2014, 10:23 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

void  swap_numbers( int &num1, int &num2)
{
    cout <<"Number one is " <<num1<<endl;
    cout<<"Number two is "<<num2<<endl;
    int temp = num2; //ex: temp = 2
    num2 = num1;   // 3 = 1
    num1 = temp;  //1= temp
    cout <<"Number one is "<<num1<<endl;
    cout <<"Number two is "<<num2<<endl;
    
}
/*
 * 
 */
int main(int argc, char** argv) {

    int num1=1;
    int num2=3;
    
    swap_numbers(num1, num2);
    
    
    return 0;
}

