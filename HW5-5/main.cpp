/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 8, 2014, 9:32 PM
 */
#include <fstream>
#include <cstdlib>
#include <iostream>
using namespace std;
double convert(double liters, double miles)
{
    const double liters_gal = .264179;
    double conversion = liters/liters_gal;
    double final =  miles/ conversion;
    
    cout <<"The number of liters to gallons is: "<<conversion<<endl;
    cout <<"The number of miles per gallon is : "<<final<<endl;
}
/*
 * 
 */
int main(int argc, char** argv) {

    
     
    cout << "Enter the number of liters consumed by a vehicle and press enter: ";
    double liters;
    cin >> liters;

    cout << "Enter the number of miles traveled by a vehicle and press enter: ";    
    double miles;
    cin >> miles;
    
   
    cout <<convert(liters, miles);
    double first;
    double second;
    ifstream in_stream;
    in_stream.open("data.txt");
    
    while(!in_stream.eof() || !in_stream.fail())
    {
        in_stream>>first >>second;
        cout <<convert(first, second);
        
        
    }

    return 0;
}

