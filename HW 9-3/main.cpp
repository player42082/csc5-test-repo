/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 7, 2014, 11:35 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int v = 42;
   int *p = &v;
    
   cout << "V "<<v<<endl;
    cout <<"V2 "<< *p<<endl;
    
    cout <<&v<< endl;
    cout <<&p<<endl;
    
    int &y = v;
    int &z =v;
    
    cout << y<<endl;
    cout << z<<endl;
    
    int temp;
     *p = temp;
     v = 12;
      temp= *p ;
         
    cout << "V "<<v<<endl;
    cout <<"V2 "<< temp<<endl;
    
    v = v+5;
    v = *p + 5;
    *p = *p +3;
    
    cout<< * p<<endl; 
    cout <<v<<endl;
    
    *p = 35 + v;
    
    
    cout<< * p<<endl; 
    cout <<v<<endl;
    
    return 0;
}

