/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on September 19, 2014, 3:01 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
string animal;
string color;
string adjective;
int num;
string another_name;
string name;
string food;

cout << "Enter a name and press enter: ";
cin >>name;
cout <<"Enter another name and press enter: ";    
cin >> another_name;
cout <<"Enter a food and press enter: ";
cin >>food;
cout<< "Enter a number between 100 and 120 and pres enter: ";
cin >> num;
cout << "Enter an adjective and press enter: ";
cin >>adjective;
cout <<"Enter a color and press enter: ";
cin>> color;
cout <<"Enter a type of animal and press enter: ";
cin >>animal;

cout <<"Dear " <<name<< "," <<endl;
cout <<"I am sorry that I am unable to turn in my homework at this time. "
<<"First, I ate a rotten " << food<< endl;
cout << "which made me turn "<<color<< " and extremely ill. I came down with a ";
cout<<"fever of "<<num<< ". Next, my "<<adjective<< endl;
cout<<" pet "<<animal<<" must have "
<<"smelled the remains of the "<<food<<" on my homework because "<<endl;
cout << "he ate it. I am currently rewriting my homework and hope you will accept it late."<<endl;

cout <<"Sincerely," <<endl;
cout <<name;

return 0;
}

