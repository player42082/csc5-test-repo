/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 25, 2014, 1:51 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout <<"Enter a word.";
    string word;
    cin >> word;
    
    bool palindrome = true;
    for (int i=0; i < word.size(); i++)
    {
        if(word[i]!= word[word.size() -1 -i])
        {
         palindrome = false; 
        }
        else
            cout << "Not a palindrome.";       
       }     
       if (palindrome)     
       {
           cout <<  "Palindrome";
       }
       else
           cout << "Not a Palindrome";
            
        
    
    return 0;
}

