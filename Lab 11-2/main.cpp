/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 24, 2014, 7:21 PM
 */
#include <vector>
#include <cstdlib>
#include <iostream>


using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

       
    int array[5];    
    int num;
    cout << "Enter 5 numbers, and -1 to quit"<< endl;
    for( int i = 1; i < 6; i++)
    {
       cout << "Enter array # "<< i <<" :";
       cin >> array[i];        
       cout << "Array "<< i << " is "<< array[i]<< endl; 
       if( array[i] == -1 )
       {
           return 0;
       }
    }
        
    return 0;
}

