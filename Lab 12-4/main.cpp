/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 4, 2014, 2:07 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int binarysearch(int array[], int size, int use_num)
{
   int low = 0;
   int high = size -1;
  
   
   while (low <= high)
   { 
       int middle_num = (low + high)/2;
   if( use_num == array[middle_num])
   {
       return middle_num;
   }   
   else if (use_num > array[middle_num])
   {
    low = middle_num+1;   
   }
   else
   {
       high = middle_num-1;
   }
}
   return -1;
}

/*
 * 
 * 
 */
int main(int argc, char** argv) {

    int array[9];
    srand(time(0));

    for(int i = 0; i < 9; i++)
    {
        array[i]= rand() % 100+1;                 
        cout << "Array "<<i << " is "<< array[i]<<endl;
    }
    
    for (int i = 0; i <= 7; i++)
    {
        for (int j = i+1; j<= 8; j++)
        {
            int temp;
            
            if (array[i]> array[j])
            {
                temp = array[i];
                array[i]= array[j];
                array[j]= temp;
            }
        }
        
    }
    
    for (int i = 0; i < 9; i++)
    {
        cout << array[i]<< "  "<<endl;
    }
    
    cout <<"User,Enter a number your searching for, ";
        cout<<    "and press enter: ";
    int user_num;
    cin >>user_num;
    
    int result = binarysearch(array, 9, user_num);
    
    if (result >= 0)
    {
        cout <<"Ur number "<<array[result]<<" was found in array "<<result;
    }
    else
    {
        cout <<"Your number is not in the array. sorry.";
    }   
    
    return 0;
}

