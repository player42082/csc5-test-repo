/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on September 30, 2014, 3:48 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
string grade;
  cout << "Enter a grade and press enter: ";
  cin >> grade;
   
  string letter = grade.substr(0,1);
    string mod = grade.substr(1);
            
    if (letter =="A")
    { 
        if(mod == "+")
            cout<< "4.0"<<endl;
        else if (mod =="-")
            cout <<"3.7"<<endl;
        else 
            cout << "4.0"<<endl;               
    }   
    
    else if (letter == "B")
    {
        if(mod =="-")
            cout << "2.7"<<endl;
        else if (mod =="+")
            cout <<"3.3"<<endl;
        else
            cout <<"3.0"<<endl;
    }
    
    else
        cout << "Error" << endl;

    //Leap Year
    
    cout << "User, please enter what year it is and press enter: ";
    int year;
    cin >>year;
    
    if( year%4 == 0 && ( year % 100 != 0 || year % 400 == 0 ))
    {
            cout << "This is a leap year!"<< endl;
    }
    else
    {
        cout << "This is not a leap year!"<< endl;
    }
    
      //guessing game
    
    int guess;
     int count = 0;
    srand(time(0));
    int rand_num = rand ()% 101;
    int x = 0;
    
    cout << "Enter a guess between 1-100  press enter: ";  
    
    do        
    
    {
        cin >> guess;
        if (rand_num < guess)
        {
            cout << "Guess to high, try again.";
            count++;
        }
        
        else if( rand_num > guess)
        {
            cout << "Guess to low, try again.";
            count++;
        }
        else
            cout << "Guess correct.";  
    }
    while (!(rand_num == guess) && (count < 10));


    return 0;
}

