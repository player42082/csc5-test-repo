/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 14, 2014, 9:42 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int convert(int feet , int inches)
{
    int feet_inches = feet * 12; //convert feet to inches
    int total_inches = feet_inches + inches; //adds  feet to inches =total inches
    
    double meters= static_cast<double> (total_inches / 39.3701);
    double centimeters= static_cast<double> (total_inches/ .393701 );
    
    cout << "Total amount converted to meters : "<< meters<<endl;
    cout <<"Total amount converted to centimeters: "<< centimeters<<endl;
}
/*
 * 
 */
int main(int argc, char** argv) {
    string answers;
    do
    {    
    cout << "Enter in the number of feet and press enter: ";
    int input_feet;
    cin >> input_feet;
    
    cout <<"Enter in the number of inches and press enter: ";
    int input_inches;
    cin >> input_inches; 
    
    convert(input_feet, input_inches);
    
    cout << "Would you like to do this again? Enter Y for yes, N for no.";
    cin >> answers;
    }
    
    while (answers=="y" || answers == "Y");
    
    return 0;
}

