/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 30, 2014, 5:17 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int answer;
    int num_pos_sum = 0;
    int num_neg_sum = 0;
    for (int i = 0; i < 10; i++)
    {
        cout <<"User, please enter 10 numbers, starting with # "<<i+1<<"  ";
        cin >> answer;
        if (answer > 0)
        {
            num_pos_sum += answer;
        }
        else
         num_neg_sum += answer;   
        
    } 
    cout <<"Sum of positive numbers is "<< num_pos_sum<<endl;
    cout <<"Sum of negative numbers is "<< num_neg_sum<<endl;
        
        
    return 0;
}

