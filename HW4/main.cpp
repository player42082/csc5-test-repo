/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on September 30, 2014, 4:13 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int answer;
    int num_pos_sum = 0;
    int num_neg_sum = 0;
    srand(time(0));
    int min = -100;
    int max = 200;
    int pos_sum = 0;
    int neg_sum = 0;
    int pos_avg;
    int neg_avg;
    for (int i = 0; i < 10; i++) {
        int num = rand() % max + min;
        cout << "Random # " << i + 1 << " is: " << num << endl;

        if (num > 0) {
            num_pos_sum += num;
            pos_sum++;

        } else {
            num_neg_sum += num;
            neg_sum++;
        }
    }
    pos_avg = num_pos_sum / pos_sum;
    neg_avg = (num_neg_sum) / neg_sum;
    cout << "Sum of positive numbers is " << num_pos_sum << endl;
    cout << "Sum of negative numbers is " << num_neg_sum << endl;
    cout << "The average of the positive numbers is " << pos_avg << endl;
    cout << "The average of the negative numbers is " << neg_avg << endl;
    //cout <<"The sum of all the neg and pos numbers is: " <<sum_all<<endl;
    //cout << "The average of all the numbers is: " << avg_all<< endl;


    //bmi prob
    double female_bmi;
    double man_bmi;
    string ans;
    int weight;
    int height;
    int age;
    string sex;
    int choc_bar = 230;

    do {
        cout << "What is your weight, please enter: ";
        cin >> weight;

        cout << "What is your height in inches, please enter: ";
        cin >> height;

        cout << "Please enter your age, and press enter: ";
        cin >> age;

        cout << "Please enter M for male, or F for female, and press enter: ";
        cin >> sex;

        if (sex == "M" || sex == "m") {
            man_bmi = 66 + (6.3 * weight) + (12.9 * height) - (6.8 * age);
            cout << "the BMI accrdingly for this male is: " << man_bmi << endl;
            double max_choc_bar = man_bmi / choc_bar;
            cout << "The max chocolate bars you can eat according to maintain  ";
            cout << "ur wieght is"<< max_choc_bar << " bars!" << endl;
        } else {
            female_bmi = 655 + (4.3 * weight)+ (4.7 * height)- (4.7 * age);
            cout << "The BMI accordingly to this  female is : " << female_bmi << endl;
            double max_choc_bar = female_bmi / choc_bar;
            cout << "The max chocolate bars you can eat according to maintain ur weight is ";
            cout << endl << max_choc_bar << " bars!" << endl;
        }
        cout << "Would you like to do this again ? Enter Y or N. ";
        cin >> ans;
    } while (ans == "Y" || ans == "y");


    cout << "User, enter the number of scores you want to input and press enter: ";
    int user_input;
    cin >> user_input;

    double points_poss_tally = 0;
    double score_tally = 0;

    for (int k = 0; k < user_input; k++) {
        cout << "Score received for exercise  # " << k + 1 << " , press enter: ";
        double score;
        cin >> score;
        score_tally += score;


        cout << "Total points possible for exercise # " << k + 1 << ", press enter: ";
        double points_poss;
        cin >>points_poss;
        points_poss_tally += points_poss;
    }
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);
    double percent = (score_tally / points_poss_tally)*100;
    cout << "Your total is  " << score_tally << " out of " << points_poss_tally << endl;
    cout << "or " << percent << " %";

    return 0;
}