/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 2, 2014, 8:20 PM
 */

#include <fstream>
#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int num;    
    
    ifstream instream1;
    instream1.open("data1.txt");    
    
    ofstream outstream;
    outstream.open("out.txt");
    
    if(instream1.fail() )
    {
        cout <<"Error opening file, try again.";
    }
    
    while (!instream1.eof());
    {
        
        instream1>>num;
        cout << num;
        //outstream<<num;
    }    
    return 0;
}

