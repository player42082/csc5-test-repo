/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 12, 2014, 7:50 PM
 */
#include <vector>
#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    vector <int> delete_num;
    
       
    delete_num.push_back(1);
    delete_num.push_back(3);
    delete_num.push_back(5);
    delete_num.push_back(6);
    delete_num.push_back(7);
    delete_num.push_back(4);
    
    
    for ( int i = 0; i < delete_num.size(); i++)
    {
    cout << "Vector number: " <<i<<"    "<< delete_num[i]<<endl;    
    }   
    
    cout << "Please select a location to delete from the vector and press ";
    cout << "enter: ";
    int vec_delete;
    cin >> vec_delete;
    
    
   // delete_num.erase(delete_num.begin() + 3); doesnt work this way
    
    for ( int i = vec_delete; i < delete_num.size()- 1; i++)
    {        
    delete_num[i] = delete_num[i + 1];    
    }
    
    delete_num.pop_back();
    
    for ( int i = 0; i < delete_num.size(); i++)
    {
    cout << "Vector number: "<<i <<"   " << delete_num[i]<<endl;    
    }   
    
    
    return 0;
}

