/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 2, 2014, 12:22 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

double convert_meters( double feet)
{
    double total_meters = feet * .3048;
    cout <<"Total amount of meters is : "<< total_meters<<endl;
    
    double total_cms = total_meters * 100;
    cout <<"Total amount of centimeters is: "<<total_cms<<endl;
}
/*
 * 
 */
int main(int argc, char** argv) {

    string answer;
        
    do
    {
    cout <<"Enter the amount of feet to convert: ";
    double feet;
    cin >> feet;
    
    cout <<"Enter the amount of inches to convert:";
    double inches;
    cin >>inches;
    
    double inch_feet = inches * .0833;
    double total_feet = inch_feet + feet;    
   
    convert_meters(total_feet);
    cout <<"Would you like to do this conversion again? Y for yes.";
    cin >>answer;
    }
    while(answer =="Y" || answer == "y");
    
    
    return 0;
}

