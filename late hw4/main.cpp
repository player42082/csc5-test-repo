/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 5, 2014, 5:57 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
  
double exec_input;
double max_score = 0;
double max_total = 0;

cout << "How many exercises to input? " ;
cin >> exec_input;
for ( int i = 0; i < exec_input; i++)

do 

{
cout <<"Score received for exercise " << i+1<<" : " ;
int test_score;
cin >> test_score;

 
cout << "Total points possilbe for exercise "<< i+1<< ": ";
int total_poss;
cin >> total_poss;

max_score += test_score;
max_total += total_poss;

cout.setf(ios::fixed);
cout.setf(ios::showpoint);
cout.precision (2);

double total_percent =  ( max_score / max_total ) * 100;

cout << "Your total is  " << max_score << " points out of "<< max_total<< endl;
cout <<"points possilbe or %" << total_percent<< endl;
} 

while (exec_input <= i); 



char answer;  
int player1 = 0;
int player2 = 0;


do
{
cout << "Welcome to the online Paper Rock Scissor game !"<< endl;

int rand_player1 = rand() % 3 + 1;         

cout << "Computer bot 1 enters his pick." <<endl;


cout << "computer bot 2 enters his pick."<< endl;

int rand_player2 =1 + rand() % 3 ; 



if (( rand_player1 == 1  ) && (rand_player2 == 2 ))
{         
    cout << "Rock breaks scissors , Good job player 1!"<<endl;
    player1++;    
} 

  else if (( rand_player1 == 2  ) && (rand_player2 == 3 ))
{    
    cout << "Scissors cuts paper , Good job player 1!"<<endl;
    player1++;    
} 

  else if (( rand_player1 == 3  ) && (rand_player2 == 1 ))
{ 
    cout << "Paper covers rock , Good job player 1!"<<endl;
    player1++;
    } 


 else if (( rand_player2 == 2   )&& (rand_player1 == 3 ))
{
    cout << "Scissors cuts paper , Good job player 2!"<<endl;
    player2++;
    }


 else if (( rand_player2 == 3   ) && (rand_player1 == 1 ))
    {
    cout << "Paper covers rock , Good job player 2!"<<endl;
    player2++;
    } 

 else if (( rand_player2 == 1   ) && (rand_player1 == 2 ))
   {
    cout << "Rock breaks scissors , Good job player 2!"<<endl;
    player2++;
    }

 else if (rand_player1 == rand_player2)
    {
    cout << "Tie between player one and player two !"<<endl;   
    }

cout << "Would you like to play again? Y for yes, or N for no: ";
cin >> answer;
}

while (answer =='Y' || answer == 'y');


cout << "Player 1 score: " <<player1 <<endl;
cout <<"Player 2 score: " <<player2;
}

