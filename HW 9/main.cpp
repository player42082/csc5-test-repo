/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 3, 2014, 9:33 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout <<"User, enter a sentence and press enter. ";
    string sentence;
    getline(cin, sentence);
    
    int num_char = 0;
    
    for (int i = 0; i < sentence.length(); i++)
    {       
        
        if(sentence.at(i)== 'a')
        {
            num_char++;
        }
    }
    cout <<"Number of characters of a is "<<num_char;
    
    return 0;
}

