/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 30, 2014, 2:49 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // syntax error
   // cout << "This is just a test ;"
    
    //logical error
    //int x =1;
    //int y = 0;
    //int y = x / y;
    
    //"Hello my name is Calvin"
    //Hi there Calvin! My name is Bill
    //Nice to meet you Bill.\n I am wondering if you're available for what?\n
    //What? Bye Calvin !\n uh... sure, bye Bill !\n\n
    //CalvinCalvinCalvinCalvinCalvin
    //BillBillBillBillBill
    
    //cout is data you want to output to the screen, requires an insertion
    //operator,and ends with a semicolon.
    
    //b) There are 7 statements
    
   // c) Identify the code that are variable declaration and initialization statements

//(Box these in the code snapshot).

//string name1; string name2;
    //d) Identify the code that is the program (Circle this in the code snapshot). 
    //Lines 6-20.
    
    //e) Can you figure out one reason how variables are useful in this example?

//Variables are useful in this example because they can be used to name and store info in memory.
    
    int JQ1, JQ2, JQ3, JQ4;

cout << "Enter four quiz scores for John and press enter: "<< endl;

cout << "Quiz 1: ";

cin >>JQ1;

cout <<"Quiz 2: ";

cin >>JQ2;

cout << "Quiz 3: ";

cin >> JQ3;

cout <<"Quiz 4: " ;

cin >> JQ4;

int MQ1, MQ2, MQ3, MQ4;

cout << "Enter four quiz scores for Mary and press enter: "<< endl;

cout << "Quiz 1: ";

cin >>MQ1;

cout <<"Quiz 2: ";

cin >>MQ2;

cout << "Quiz 3: ";

cin >> MQ3;

cout <<"Quiz 4: " ;

cin >> MQ4;

int MaQ1, MaQ2, MaQ3, MaQ4;

cout << "Enter four quiz scores for Matthew and press enter: "<< endl;

cout << "Quiz 1: ";

cin >>MaQ1;

cout <<"Quiz 2: ";

cin >>MaQ2;

cout << "Quiz 3: ";

cin >> MaQ3;

cout <<"Quiz 4: " ;

cin >> MaQ4;

cout<< "Name"<<" Quiz1 "<<" Quiz2 "<< " Quiz3 " << " Quiz3 "<<endl;

cout<< "John "<<JQ1 << " "<<JQ2<<" "<<JQ3<<" "<<JQ4<< endl;

cout<< "Mary "<<MQ1 << " "<<MQ2<<" "<<MQ3<<" "<<MQ4<< endl;

cout<< "Matthew"<<MaQ1 << " "<<MaQ2<<" "<<MaQ3<<" "<<MaQ4<< endl;

//4) Write a program that plays the game of Mad Lib. Your program should prompt the user to enter the following strings: For simplicity, each input can only be a max of 10 characters.

//a. A name

//b. Another name

//c. A food

//d. A number between 100 and 120

//e. And Adjective

//f. A color

//g. An animal

//After the strings are input, the should be substituted into the story below and output to the console.

string animal;

string color;

string adjective;

int num;

string another_name;

string name;

string food;

cout << "Enter a name and press enter: ";

cin >>name;

cout <<"Enter another name and press enter: ";

cin >> another_name;

cout <<"Enter a food and press enter: ";

cin >>food;

cout<< "Enter a number between 100 and 120 and pres enter: ";

cin >> num;

cout << "Enter an adjective and press enter: ";

cin >>adjective;

cout <<"Enter a color and press enter: ";

cin>> color;

cout <<"Enter a type of animal and press enter: ";

cin >>animal;

cout <<"Dear " <<name<< "," <<endl;

cout <<"I am sorry that I am unable to turn in my homework at this time. "

<<"First, I ate a rotten " << food<< endl;

cout << "which made me turn "<<color<< " and extremely ill. I came down with a ";

cout<<"fever of "<<num<< ". Next, my "<<adjective<< endl;

cout<<" pet "<<animal<<" must have "

<<"smelled the remains of the "<<food<<" on my homework because "<<endl;

cout << "he ate it. I am currently rewriting my homework and hope you will accept it late."<<endl;

cout <<"Sincerely," <<endl;

cout <<name;


    
    return 0;
}

