/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 1, 2014, 10:46 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

double max(double x, double y) //function that compares x to y
{
    if (x < y) //if value x is bigger than value y
    {
        cout << "Number two (" << y << ") is bigger than number one (" << x << ")" << endl;
    }
    else {
        cout << "Number one (" << x << ") is bigger than number two (" << y << ")" << endl;
    }
}

double i_cream(double ppl, double weight)// function ice cream
{
    double per = weight / ppl; //divides the weight ice cream by people
    cout << weight << " lbs of ice cream split between " << ppl << " people, ";
    cout << "each person can have up to " << per << " lbs portions."<<endl;
}

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Please enter #1 for random number comparison function or ";
    cout << "enter #2 for ice cream function.";
    int num;
    cin >>num;
    string answer;

    switch (num) {
        case 1:
        {
            string num;
        }


        do{
            srand(time(0)); //generates random numbers
            double random1 = rand() % 100; //random numbers 0 -100
            double random2 = rand() % 100; //random numbers 0 -100

            max(random1, random2);

            double random3 = rand() % 100; //random numbers 0 -100
            double random4 = rand() % 100; //random numbers 0 -100

            max(random3, random4);
            cout << "Would you like to do it again? Enter y for yes.";
             cin >>answer;
        }
        while (answer == "Y" || answer == "y");

            break;

        case 2:

            do
            {
            cout << "Enter the number of customers and press enter: ";
            double customers; //declare customers
            cin >> customers; //cin customers

            cout << "Enter the weight of ice cream and press enter: ";
            double ice_cream; //declare ice cream
            cin >> ice_cream; //cin ice cream

            i_cream(customers, ice_cream);
           cout << "Would you like to do it again? Enter y for yes.";
           cin >>answer;
        }
        while (answer == "Y" || answer == "y");
            
            
            return 0;
    }
}
