/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 19, 2014, 2:21 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {
    string answer;    
    srand(time(0)); 
    int total_score = 0;
    int player1_die = 0;
    int player1_die_temp = 0;
        
    int die1 = rand() %7+1;
    int die2 = rand() %7+1;    
    int sum = 0;
    int tempsum = 0;    
    
    cout << "First die is " << die1 <<endl;
    cout << "Second die is "<< die2 <<endl;    
     sum = die1 + die2;
     tempsum += sum;   
     cout << "          The sum of your first dice roll is "<< sum <<endl;     
    
     if ((sum == 7) || (sum ==11))
     {
    cout << "You win ! Game over !";
     player1_die +=10;
     return 0;
    }
    
    if ((sum == 2) || (sum == 3) || (sum == 12))
    {
     cout << "You lose, 2, 3, or 12 bust!";     
      player1_die -=10;
     return 0;
    }         
     int i = 1;
     
     
     do
     {           
    die1 = rand() % 7+1;
    die2 = rand() % 7+1; 
    sum = 0;
    cout << "Roll number "<< i << " for die number 1 is " << die1 <<endl;
    cout << "Roll number  "<<i << " for die number 2 is "<< die2 <<endl;    
     sum = die1 + die2;
     cout << "       The total sum of roll number " << i <<"  is "<< sum <<endl;
     
     if (tempsum == sum)
     {
      cout <<"Your second roll tally matches your first roll tally, You win!!! ";
      player1_die +=10;
      return 0;
     }
     
     if (sum == 7) 
     {
    cout << "You lose ! Game over !"; 
    player1_die -=10;
    return 0;
     }     
     i ++;           
     }     
     while (( sum != tempsum) || (sum != 7));     
     
      total_score = player1_die +  player1_die_temp;
     
     cout << "Your points left are "<< total_score <<endl;
    
     return 0;
}    