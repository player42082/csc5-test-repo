/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 2, 2014, 11:40 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "How many apples do you want?" << endl;
    
    int num_Apples;
    
    cin >> num_Apples;
    
    cout << "How many people are there?" << endl;
    
    int num_ppl;
    
    cin >> num_ppl;
    
    cout << "Each person should get " << num_Apples/num_ppl <<" apples per person!";
    
    return 0;
}

