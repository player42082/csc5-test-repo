/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on September 24, 2014, 9:23 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{

    
    int year;
    int part;
    string answer;
    do
    {    
     string roman;
    cout <<"Enter a year to convert to Roman Numerals and please press enter: ";
cout <<endl;
    cin >> year;
    
    if ((year > 3000) || (year < 1000))
        {
        cout <<"Year entered is not within proper range"<<endl;
        }    
    else if ((year <=3000) || (year>=1000) ) 
        {        
        if (year >= 1000)
            {            
            part = (year /1000);            
            for (int i = 0; i < part; i++)
                {
                roman += 'M';
                }
                year %= 1000;          
            }
        
        if (year >= 100)
            {
              part = (year/100);              
              if (part == 9)
                {
                  roman += "CM";
                }              
              else if (part >=5)
                {
                  roman += 'D';                  
                  for (int i = 0; i < part - 5; i++)
                    {
                      roman += 'C';
                    }                  
                }              
              else if (part == 4)
                {
                  roman+="CD";
                }             
              else if (part == 1)
                {
                  for(int i = 0; i < part; i++)
                  {
                      roman += 'C';
                  }                  
                }
             year %= 100;
            }        
        if (year >= 10)
            {
                part = (year / 10);                
                if (part == 9)
                    {
                    roman += "XC";
                    }                
                else if (part >= 5 )
                    {
                    roman += 'L';                    
                    for(int i = 0; i < part - 5; i++)
                        {
                        roman += 'X';                        }
                    }
                
                else if (part==4)
                    {
                    roman += "XL";
                    }                
                else if (part >= 1)
                    {
                    for(int i=0; i < part; i++)
                        {
                        roman += 'X';
                        }
                    }
                year %= 10;
            }
        
        if (year >= 1)
            {
            part = year;                    
            if (part==9)
                {
                    roman += "IX";
                }
            
            else if (part >=5)
                {
                    roman += 'V';                         
                    for (int i = 0; i < part - 5; i++)
                        {
                            roman += 'I';
                        }
                }
            
            else if (part == 4)
                {
                    roman += "IV";
                }
            
            else if (part >= 1)
                {
                for (int i = 0; i < part; i++)
                    {
                    roman += 'I';
                    }
                }
           

 }
    }cout <<"Roman Numeral: "<<roman<<endl;
        
        cout <<"do again? Enter y for yes and n for no."<<endl;
        cin >> answer;
   
    }while("y" == answer || "Y" == answer);
}

