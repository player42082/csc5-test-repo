/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 25, 2014, 1:25 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "User, enter a sentence and press enter: ";
    
    char s;
    char symbol;
    
    ofstream outfile;  
    
    outfile.open("text.txt");

    do {
        
        outfile << s;   
        
        cin.get(symbol);
        
    } while ( symbol != '@');

    outfile.close();
    return 0;
}

//getline(cin, s);
//Describe the differences between cin.get(), cin >>, and getline(cin, string
//cin>> takes in input connected to your keyboard, The data is temp, when the
//prog ends, the data typed in go away. Files store the data and remain, to be
//used over , and over again and accessed by other programs.