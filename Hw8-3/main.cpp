/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 16, 2014, 7:11 PM
 */

#include <vector>
#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int size = 6; 
    vector <int> delete_num;    
       
    delete_num.push_back(1);
    delete_num.push_back(3);
    delete_num.push_back(5);
    delete_num.push_back(6);
    delete_num.push_back(7);
    delete_num.push_back(4);    
    
    for ( int i = 0; i < delete_num.size(); i++)
    {
    cout << "Vector number " <<i<<"    "<< delete_num[i]<<endl;    
    }   
    
    cout << "Please select a number to delete from the vector and press ";
    cout << "enter: ";
    int vec_delete;
    cin >> vec_delete;
    
    int number_location;   
    
    
   // delete_num.erase(delete_num.begin() + 3); doesnt work this way
    
    for ( int i = 0; i < size; i++)
    {        
   if (delete_num[i] == vec_delete)
   {
        number_location= i; 
   }
    }
    
    //delete_num.pop_back();
    
    for ( int i = number_location; i < size -1; i++)
    {
        delete_num[i] = delete_num [i + 1];       
    }   
    size --;
    
    for (int i = 0; i < size ; i++)
    {
        cout << "Vector # " <<i  << "  "<< delete_num[i]<< endl;
    }
    
    return 0;
}

