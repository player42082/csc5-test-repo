/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 8, 2014, 1:48 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    srand (time(0));
    int *dArray;
    cout<<"User, enter a number for the size of array you want and press enter:";
    int user;
    cin >>user;
    dArray = new int[user];
     
    
    for (int i = 0; i < user; i++)
    {        
         dArray[i] = rand() % 100+1; 
        cout << "Array "<<i+1 << " is "<< dArray[i]<<endl;
        
    }
    
    return 0;
}

