/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 14, 2014, 8:23 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int convert(int pounds, int ounces) {
    int lbs_oz = pounds * 16; //convert lbs to oz
    int total_oz = ounces + lbs_oz; // converts all to oz

    double oz_grams = static_cast <double> (total_oz * 28.3495); // converts oz to gram conversion
    double oz_kg = static_cast <double> (oz_grams * .001); //converts grams to kg
    
    cout << "Total number of grams is :" << oz_grams <<endl;
    cout << "Total number of kilograms is : "<< oz_kg <<endl;
}

/*
 * 
 */
int main(int argc, char** argv) {
    string answer;
    
    
    do
    {
    cout << "Please enter the weight in lbs first, then in ounces of the object and then ";
    cout << "press enter: ";
    cout << "Lbs : ";
    int lbs;
    cin >> lbs;

    cout << "Oz : ";
    int oz;
    cin >> oz;

    convert(lbs, oz);
    
    cout << "Would you like to do this again?";
    cin >> answer;
    }
    while (answer == "y" || answer == "Y");


    return 0;
}

