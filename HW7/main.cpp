/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 25, 2014, 4:39 PM
 */
#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;
/*
 * 
 */
int main(int argc, char** argv) {
    //cout << "User, enter a file name and press enter (15 max size): "; 
    //char file_name [16];
    //cin >> file_name;       
    int sum = 0;
    int loop =0;
    ofstream outfile1;
    ofstream outfile2;
    ofstream outfile3;
    
    outfile1.open("data1.txt");
    outfile2.open("data2.txt");
    outfile3.open("data3.txt");
    
    ifstream infile;
    infile.open("data.txt" );// ios::app

     if (infile.fail( ))
     {
         cout <<"Input file opening failed !"<<endl;
         //cin >> file_name;
         exit(1);         
     }
    int odd_nums = 0;
    int even_nums = 0;
    int zero_nums =0;
    int numbers;
    vector <int> val;
    
    while ( ! infile.eof() )
    {
        infile >> numbers; 
        val.push_back(numbers);
    }        
    
    for (int i= 0; i <val.size(); i++)
    {
        if (val[i] % 2 ==0)
        {    
            outfile1 << val[i]<<"   "; 
            even_nums ++;
        }
        
        if (val[i] == 0)
        {
            outfile2 << val[i]<<"  ";
           zero_nums ++; 
        }        
        
        if (val[i]% 2 == 1)
        {
            outfile3<<val[i]<<"   ";
            odd_nums++;
        }
        
    }
       sum += numbers;
    outfile3<< sum;
    
    infile.close();
    outfile1.close();
    outfile2.close();
    outfile3.close();   
    
    cout <<"There are "<<zero_nums<< " zero numbers and "<<even_nums;
        cout    << " even numbers. And there are "<<odd_nums<<" odd numbers.";
    
    return 0;
    
}

     //infile.get(next);
