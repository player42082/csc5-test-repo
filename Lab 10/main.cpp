/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 19, 2014, 11:46 PM
 */

#include <cstdlib>
#include <iostream>

#include <vector>
#include <fstream>
using namespace std;    
            

/*
 *
    while ( ! infile.eof() )
    {
        infile >> numbers; 
        val.push_back(numbers);
    }       
 */
int main(int argc, char** argv) {

   ifstream in_stream;
   in_stream.open("in_file.txt");
   
   if (in_stream.fail())
   {       
       cout << "Error !";
   }
   
    vector <int> file_num;
    int numbers;
    
    while ( ! in_stream.eof())
    {
        in_stream >> numbers;
        file_num.push_back(numbers);
        cout << numbers <<"  ";
    }   
   
    return 0;
}