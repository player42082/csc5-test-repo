/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 2, 2014, 8:33 PM
 */

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    srand(time(0));
    vector<int> v;
    int count_odd = 0;
    int large_num = 0;
    for (int i = 0; i < 20; i++) {
        int random = rand() % 100 + 1;
        v.push_back(random);
        cout << v[i] << endl;

        if (v[i] % 2 != 0) {
            count_odd++;
        }
        if (large_num < v[i]) {
            large_num = v[i];
        }
    }
    cout << "The number of odd numbers is " << count_odd<<endl;
    cout << "The highest number in this vector is "<<large_num;
    return 0;
}

