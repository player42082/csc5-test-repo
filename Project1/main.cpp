/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 17, 2014, 9:07 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

int craps() { //calls the craps game function
        string answer;
        srand(time(0));   //initializes variables to zero to begin keeping score
        int total_score = 0;
        int player1_die = 0;
        int player1_die_temp = 0;

        int die1 = rand() % 7 + 1;
        int die2 = rand() % 7 + 1;  //implements random integers for craps game
        int sum = 0;
        int tempsum = 0;

        cout << "First die is " << die1 << endl;
        cout << "Second die is " << die2 << endl; //sees out random die #'s
        sum = die1 + die2; //sees out the sum of the random die numbers
        tempsum += sum;  //keeps account of first tally of dice, for round 2
        cout << "          The sum of your first dice roll is " << sum << endl;
                                            
        if ((sum == 7) || (sum == 11)) { //if you rolled a 7 or 11, u win
            cout << "You win ! Game over !";
            player1_die += 10; //u win 10  to your bet
            return 0; //ends game
        }

        if ((sum == 2) || (sum == 3) || (sum == 12)) {// if u roll this, u lose
            cout << "You lose, 2, 3, or 12 bust!";
            player1_die -= 10; //u lose 10 from your bet
            return 0; //ends game
        }
        int ii = 1; //this is the first round, any other rounds are accumulated


        do { //do while loop lets prog run until someone loses
            die1 = rand() % 7 + 1; //random die play 1-6
            die2 = rand() % 7 + 1;
            sum = 0;  //each time prog runs, the sum count goes back to 0
            cout << "Roll number " << ii << " for die number 1 is " << die1 << endl;
            cout << "Roll number  " << ii << " for die number 2 is " << die2 << endl;
            sum = die1 + die2; //sees out the dice and its sum
            cout << "       The total sum of roll number " << ii << "  is " << sum << endl;

            if (tempsum == sum) { //if ur first roll  is rolled again, u win
                cout << "Your second roll tally matches your first roll tally, You win!!! ";
                player1_die += 10; //u win a 10 chips back
                return 0; //ends the game
            }

            if (sum == 7) { //if you roll a 7 u lose
                cout << "You lose ! Game over !";
                player1_die -= 10; //u lose ur 10 chips
                return 0; //ends the game
            }
            ii++; //adds another point to how many rounds, and repeats again
        } while ((sum != tempsum) || (sum != 7));//prog runs until someone wins

        total_score = player1_die + player1_die_temp;//tallys up the score

        cout << "Your points left are " << total_score << endl;// sees out score
         return 0;  
}


int royaltycard(int player1_card, int player2_card, int & i, int & k) {
    if (player1_card == 12) {   //if cards are 12, 13, 14, they are J, Q, K
        cout << "Player 1 got a Jack ! Worth 10!" << endl;
    } else if (player1_card == 13) {
        cout << "Player 1 got a Queen ! Worth 10!" << endl;
    } else if (player1_card == 14) {
        cout << "Player 1 got a King ! Worth 10!" << endl;
    } else if (player2_card == 12) {
        cout << "Dealer got a Jack ! Worth 10!" << endl;
    } else if (player2_card == 13) {
        cout << "Dealer got a Queen ! Worth 10!" << endl;
    } else if (player2_card == 14) {
        cout << "Dealer got a King ! Worth 10!" << endl;
    }
}

int cardfunction(int player1_card, int player2_card, int & i, int & k) {//calls 
    i = 0;                                                      //function
    k = 0;          //initializes numbers to 0
    int temp1 = 0;
    int temp2 = 0;
    int tempi = 0;
    int tempk = 0;

    if (player1_card > 11) { //if number over 11,it's a royal card,call function
        royaltycard(player1_card, temp2, i, k); //player1 function call
        i += 10;    //u win 10 to ur chips     
    } else if (player1_card <= 10) {// else if card is less or equal to 10
        i += player1_card;          //add that number to tally
        cout << "Player1's  card  " << i << endl; //sees out number < 11
    }

    if (player1_card == 11) {//if number is 11, its played as an Ace
        cout << "Player1 you got an Ace ! Do you want to use it as a 1 or 11?";
        int answer;  //prompts player if to use ace as 1 or 11
        cin >>answer;
        if (answer == 1) { //if used as 1, we add one
            cout << "You chose to use it as a one." << endl;
            i += 1;  
        } else if (answer == 11) {
            i += 11;//if used as 11, we add eleven
        }
    }


    if (player2_card > 11) { //if dealers card over 11, its a royalty card
        royaltycard(temp1, player2_card, i, k); //player2 function call
        k += 10; //add 10 to dealers points        
    } else if (player2_card <= 10) {//if dealers cards le then 10, we add to sum
        k += player2_card;  //see out the dealers hand
        cout << "Dealers card " << k << endl;
    }

    if (player2_card == 11) { //if dealer gets an ace, we use it accordingly
        cout << "Dealer got an Ace!"; 
        if (tempk < 11) {//if dealer tally is less than 11,dealer uses ace as 11
            cout << "Dealer protocol to use ace as a eleven." << endl;
            k += 11;
        } else if (tempk > 17) {
            k += 1;//if dealer tally is greater then 17, ace is a 1
            cout << "Dealer protocol to use ace as a one." << endl;
        }

        tempi += i;  //adds the players card values, to compare
        tempk += k;  //adds the dealers card values , to compare
    }
}

int blackjack() {  //calls the function blackjack game
        srand(time(0));  //generates rand numbers
        int run_time = 0;  //keeps track of how many time the prog runs
        string answers;  
        int i; // i and k are the players and dealers card values
        int k;
        int m = 0;
        int tempi = 0;  
        int tempk = 0;

        do {
            int player1_card = rand() % 14 + 2; //player1 random card 
            int player2_card = rand() % 14 + 2; //player2 random card
            cardfunction(player1_card, player2_card, i, k);
            tempi += i;  // player 1 score
            tempk += k;  //dealer score
            run_time++;
        } while (run_time < 2);// do while loop allows dealer/player get 2 cards

        cout << "Player 1 total tally is " << tempi << endl; //tallys points for
        cout << "Dealer total tally is " << tempk << endl;  //player and dealer

        if (tempk <= 10 && tempk < tempi); //||
        {
            cout << "Dealer hits again, to make it a fair game!" << endl;
            int player2_card = rand() % 14 + 2; //player2 hits again when his  
            cardfunction(m, player2_card, i, k);//cards less than 10 or less than
            tempk += k;                         //player
            cout << "Dealer total tally is " << tempk << endl;
        }

        if (tempi > 21) {  //if player is over 21, he loses
            cout << "Player 1 total tally is " << tempi << endl;
            cout << "Dealer total tally is " << tempk << endl;
            cout << "Bust ! Game over player 1. You lose";
            return 0;
        } else if (tempk > 21) {//if dealer is over 21, bust
            cout << "Player 1 total tally is " << tempi << endl;
            cout << "Dealer total tally is " << tempk << endl;
            cout << "Bust ! Game over. Dealer loses.";
             return 0;
        }


        if (tempk == 21) { //if dealer is equal to 21, u lose
            cout << "Dealer wins ! Game over.";
            cout << "Player 1 total tally is " << tempi << endl;
            cout << "Dealer total tally is " << tempk << endl;
            return 0;        
        } else if (tempi == 21) { //if player is equal to 21, he wins
            cout << "Player1 wins. You win !";
            cout << "Player 1 total tally is " << tempi << endl;
            cout << "Dealer total tally is " << tempk << endl;
       return 0; }  

        if (tempi == tempk) { //if player and dealer tie, dealer wins
            cout << "Tie between player 1 and dealer, Dealer wins !" << endl;
            cout << "Player 1 total tally is " << tempi << endl;
            cout << "Dealer total tally is " << tempk << endl;
             return 0;
        } 

        cout << "Would you like a hit player 1? Enter Y for yes. ";
        cin >> answers;  //option to let player hit
        if (answers == "y" || answers == "Y") {
            int player1_card = rand() % 14 + 2; //player1 random card     
            cardfunction(player1_card, m, i, k);
            tempi += i;
            cout << "Player 1 total tally is " << tempi << endl;
            cout << "Dealer total tally is " << tempk << endl;
        } else if (tempi > 21) {
            cout << "Bust! Game over. You lose." << endl;
             return 0; //if player goes over 21, he loses
        }

        if (tempk <= 10 && tempk < tempi); 
        {
            cout << "Dealer hits again, to make it a fair game!" << endl;
            int player2_card = rand() % 14 + 2; //dealer hits if lower than player   
            cardfunction(m, player2_card, i, k);// or lower than 10
            tempk += k;
            cout << "Dealer total tally is " << tempk << endl;
        }

        if (tempi > 21) {  //if player is over 21, bust and loses
            cout << "Player 1 total tally is " << tempi << endl;
            cout << "Dealer total tally is " << tempk << endl;
            cout << "Bust ! Game over player 1. You lose";
             return 0;
        } else if (tempk > 21) {//if dealer is over 21, bust and loses
            cout << "Player 1 total tally is " << tempi << endl;
            cout << "Dealer total tally is " << tempk << endl;
            cout << "Bust ! Game over. Dealer loses.";
              return 0;
        }


    }



int main(int argc, char** argv) {
    
    int player_points =100;
    string user_answer;
    do
    {
    cout << "Welcome to the online Blackjack and Craps game. " << endl;
    cout << "enter number 1 for Blackjack, or enter 2 for Craps.";      
    int num;  //Menu for user to select what game he wants to play
    cin >> num;

    switch(num) //calls the game to play
        {
        case 1: blackjack(); break;      
              
                 
        case 2: craps(); break;
         }    
    
    }
         while ((player_points == 0) || (user_answer == "y") || (user_answer == "Y"));   
    
    }


    


    

   
    