/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on October 15, 2014, 1:46 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

void compute_coins(int coin_value,  int& amount_left)
{  
    const int quarter = 25;
    const int dime = 10;
    const int penny = 1;        
    
   int total_quarters = coin_value / quarter ; //coin value divided by quarters
   cout << "Total amount of quarters is "<<total_quarters<< endl;
   amount_left = coin_value -(coin_value / quarter *25 ); //amount left after_quarters      
   
   int total_dimes = amount_left / dime;   //divides amount left by dimes
   cout <<"Total amount of dimes is "<<total_dimes<<endl;
   amount_left = amount_left % dime;
   
   int total_pennys = amount_left /penny;
   cout <<"Total amount of pennys is "<< total_pennys<<endl;       
}
/*
 * 
 */
int main(int argc, char** argv) {
    string answer;
    
    do
    {    
    cout << "Please enter the amount of change you have and press enter: ";
    int change;
    cin >> change;    

    int amount;
    
    compute_coins(change,   amount);  
    
    
    cout << "Please enter the amount of change you have and press enter: ";    
    cin >> change;    
    
    compute_coins(change,   amount); 
    
    
    cout <<"Would you like to do this again? Y for y, N for no.";
    cin >>answer;
    }
    while (answer == "Y" || answer == "y");
    
    return 0;
}

