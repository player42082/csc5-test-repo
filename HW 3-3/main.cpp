/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 30, 2014, 4:57 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    cout << "Enter the amount of people to attend the meeting, press enter: ";
    int people;
    cin >> people;
    
    if (people < 200)
    {
        int protocol = 200 - people;
        cout << "You are in accordance with fire code procedure, you can have ";
        cout << protocol <<" more people attend.";
    }
    
    if (people > 200)
    {
        int outofprotocol =   people- 200;
        cout << "You are NOT in accordance with fire code procedure, You must ";
        cout  <<" comply. You have to uninvite "<< outofprotocol<<" poeple.";
    }
    
    if (people == 200)
    {       
        cout << "You are in accordance with fire code procedure.";
        cout << "You can not invite anymore people to attend.";
    }
    
    return 0;
}

