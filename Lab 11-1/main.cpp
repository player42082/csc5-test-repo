/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on November 23, 2014, 1:00 AM
 */

#include <cstdlib>
#include <iostream>
#include <vector>

using namespace std;

/*
 * 11-1
 */
int main(int argc, char** argv) {

    vector<int> vec_random_nums;
    vec_random_nums[10];
    srand (time(0));  
   
    int max =100;
   
    
    for (int i = 0; i < 10; i++ )
    {
         int random = rand() % max ;
         //random>> vec_random_nums[i];
        vec_random_nums.push_back(random);
        cout << "vector # "<<i <<" is "<<vec_random_nums[i]<<endl;
    }    
      
    
    int array[10];
   
    for(int i = 1; i < 11; i++)
    {
         
        array[i] = rand() % max ;    //rand() % max; 
        cout << "Array # " <<i <<" is "<<array[i]<< endl;
    }
    
    
    return 0;
}

