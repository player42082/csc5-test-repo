/* 
 * File:   main.cpp
 * Author: Zaragoza-Castellano
 *
 * Created on December 3, 2014, 11:55 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*1)	Implement the bubble sort algorithm from last week’s lecture 
 * for an array.
 * 
 */
int main(int argc, char** argv) {
    int array[9];
    srand(time(0));

    for(int i = 0; i < 9; i++)
    {
        array[i]= rand() % 20+1;                 
        cout << "Array "<<i << " is "<< array[i]<<endl;
    }
    
    for (int i = 0; i <= 7; i++)
    {
        for (int j = i+1; j<= 8; j++)
        {
            int temp;
            
            if (array[i]> array[j])
            {
                temp = array[i];
                array[i]= array[j];
                array[j]= temp;
            }
        }
        
    }
    
    for (int i = 0; i < 9; i++)
    {
        cout << array[i]<< "  ";
    }
    
    return 0;
}

